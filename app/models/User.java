package models;


import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import play.db.jpa.Model;

import java.util.List;


@Entity
public class User extends Model 
{
	public String firstName;
	public String lastName;
	public String electorialDistrict;
	
	@OneToMany(mappedBy = "from", cascade=CascadeType.ALL)
	  List<Donation> donations = new ArrayList<Donation>();
	
	public String email;
	public String password;
	public String ageRange;
	public String gender;
	public String address;
	
	
	public User( 
	    String firstName, 
	    String lastName,
	    String electorialDistrict,
			String email, 
			String password, 
			String ageRange, 
			String gender, 
			String address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.electorialDistrict = electorialDistrict;
		this.email = email;
		this.password = password;
		this.ageRange = ageRange;
		this.gender = gender;
		this.address = address;
	}
	
	public static User findByEmail(String email) {
		return find("email", email).first();
	}

	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}
}



