package models;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import play.db.jpa.Model;

import java.util.List;

@Entity
public class Admin extends Model{
	
	public String userName;
	public String adminPassword;
	
	public static Admin findByuserName(String userName)	{
		return find("userName", userName).first();
	}
	
	public boolean checkPassword(String adminPassword)	{
		return this.adminPassword.equals(adminPassword);
	}

}
