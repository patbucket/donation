package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import play.db.jpa.Blob;
import play.db.jpa.Model;

import java.util.List;

@Entity
public class Candidate extends Model {
	
	public String candFirstName;
	public String candLastName;
	public String email;
	public String candMessage;
	public String candBio;
	public Blob thumbnailPicture;
	public Blob candPicture;
	
	
	//use candidate email to identify candidate
	public static Candidate findByEmail(String email) {
    return find("email", email).first();
  }

}


