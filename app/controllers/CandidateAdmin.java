package controllers;

import play.*;
import play.db.jpa.Blob;
import play.mvc.*;

import java.util.*;

import models.*;

//allow admin to add a candidate,image, thumbnail image and edit candidate details
public class CandidateAdmin extends Controller
{

  // render admin page and list all candidates in database
  public static void index()
  {
    List<Candidate> candidates = Candidate.findAll();
    render(candidates);
  }

  // adds candidate to database
  public static void register(Candidate candidate)
  {
    candidate.save();
    index();
  }

  // get candidate id and log name to console
  public static void candId(Long id)
  {
    Candidate candidateId = Candidate.findById(id);
    Logger.info("Editing Candidate " + candidateId.candFirstName + ' ' + candidateId.candLastName);
    editCandidate(candidateId);
  }

  // render page that allows editing of selected candidates details
  public static void editCandidate(Candidate candidate)
  {
    // put into the session object the candidate email at the key candidateEdit
    session.put("candidateEdit", candidate.email);
    render(candidate);
  }

  // edit selected candidate details
  public static void editDetails(String candFirstName, String candLastName, String email, String candMessage,
      String candBio)
  {
    Candidate candidate = Candidate.findByEmail(session.get("candidateEdit"));
    candidate.candFirstName = candFirstName;
    candidate.candLastName = candLastName;
    candidate.email = email;
    candidate.candMessage = candMessage;
    candidate.candBio = candBio;
    candidate.save();
    index();
  }

  // upload candidate picture
  public static void uploadPicture(String email, Blob picture)
  {
    // ask the session for the value corresponding to the key candidateEdit
    Candidate candidate = Candidate.findByEmail(session.get("candidateEdit"));
    candidate.candPicture = picture;
    candidate.save();
    index();
  }

  // upload candidate thumbnail image
  public static void uploadThumbnail(String email, Blob picture)
  {
    Candidate candidate = Candidate.findByEmail(session.get("candidateEdit"));
    candidate.thumbnailPicture = picture;
    candidate.save();
    index();
  }

  // render admin report listing all candidates and users
  public static void adminReport()
  {
    List<User> users = User.findAll();
    List<Candidate> candidates = Candidate.findAll();
    render(users, candidates);
  }
}
