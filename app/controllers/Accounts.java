package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller
{
  public static void index()
  {
    render();
  }

  public static void login()
  {
    render();
  }

  public static void signup()
  {
    render();
  }

  public static void logout()
  {
    session.clear();
    index();
  }

  public static void register(String firstName, String lastName, String electorialDistrict, String email, String password,
      String ageRange, String gender, String address)
  {
    Logger.info(firstName + " " + lastName + " " + email + " " + password + " " + ageRange);

    User user = new User( firstName, lastName, electorialDistrict, email, password, ageRange, gender, address);
    user.save();

    {
      login();
    }
  }

  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" + password);

    User user = User.findByEmail(email);
    if ((user != null) && (user.checkPassword(password) == true))
    {
      Logger.info("Authentication successful of " + user.firstName + " " + user.lastName);
      session.put("logged_in_userid", user.id);
      CandidatePublic.index();
    }
    else
    {
      Logger.info("Authentication failed");
      login();
    }

  }

  public static User getCurrentUser()
  {
    String userId = session.get("logged_in_userid");
    if (userId == null)
    {
      return null;
    }
    User logged_in_user = User.findById(Long.parseLong(userId));
    Logger.info("In Accounts controller: Logged in user is " + logged_in_user.firstName);
    return logged_in_user;
  }

  // render page to edit logged in users details
  public static void editProfile()
  {
    User user = getCurrentUser();
    render(user);
  }

  // edit logged in users details and return to donation page
  public static void change(String firstName, String lastName, String email, String ageRange, String gender,
      String address)
  {
    User user = getCurrentUser();
    user.firstName = firstName;
    user.email = email;
    user.ageRange = ageRange;
    user.gender = gender;
    user.address = address;
    user.save();
    CandidatePublic.index();
  }

}
