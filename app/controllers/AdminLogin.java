package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class AdminLogin extends Controller
{

  public static void index()
  {
    render();
  }

  public static void authenticateAdmin(String userName, String adminPassword)
  {
    Logger.info("Attempting to authenticate with " + userName + ":" + adminPassword);

    Admin admin = Admin.findByuserName(userName);
    if ((admin != null) && (admin.checkPassword(adminPassword) == true))
    {
      Logger.info("Authentication successful of " + admin.userName);
      session.put("logged_in_userid", admin.id);
      CandidateAdmin.index();
    }
    else
    {
      Logger.info("Authentication to admin failed");
      Welcome.index();
    }

  }

}
