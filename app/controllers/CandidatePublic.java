package controllers;

import play.*;
import play.db.jpa.Blob;
import play.mvc.*;

import java.util.*;

import models.*;

public class CandidatePublic extends Controller
{

  // render candidates page listing all candidates in database
  public static void index()
  {
    List<Candidate> candidates = Candidate.findAll();
    render(candidates);
  }

  // get candidate id and log name to console
  public static void candIdPublic(Long id)
  {
    Candidate candidateId = Candidate.findById(id);
    Logger.info("Viewing Candidate " + candidateId.candFirstName + ' ' + candidateId.candLastName);
    candidateInfo(candidateId);
  }

  // render page to view selected candidates details
  public static void candidateInfo(Candidate candidate)
  {
    // put into the session object the candidate email at the key candidateInfo
    session.put("candidateInfo", candidate.email);
    render(candidate);
  }

  // get candidate picture
  public static void getPicture(String email)
  {
    // ask the session for the value corresponding to the key candidateInfo
    Candidate candidate = Candidate.findByEmail(session.get("candidateInfo"));
    Blob picture = candidate.candPicture;
    if (picture.exists())
    {
      response.setContentTypeIfNotSet(picture.type());
      renderBinary(picture.get());
    }
  }

  // get candidate thumbnail image and display if it exists
  public static void getThumbnail(Long id)
  {
    Candidate candidate = Candidate.findById(id);
    Blob picture = candidate.thumbnailPicture;
    if (picture.exists())
    {
      response.setContentTypeIfNotSet(picture.type());
      renderBinary(picture.get());
    }
  }

}
